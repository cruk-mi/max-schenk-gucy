## Code for the manuscript "Soluble Guanylate Cyclase Signalling Mediates Etoposide Resistance in Progressing Small Cell Lung Cancer" 

RNA-seq data has been deposited in the EMBL-EBI ArrayExpress database under accession code E-MTAB-8465, titled ‘RNA of Small Cell Lung Cancer Circulating Tumor Cells Derived Explants’, (http://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-8465) 

WES data has been deposited under accession code E-MTAB-10880, titled ‘Soluble Guanylate Cyclase Signalling Mediates Etoposide Resistance in Progressing Small Cell Lung Cancer’, (https://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-10880/) 
