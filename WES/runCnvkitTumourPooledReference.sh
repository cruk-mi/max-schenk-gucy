#!/usr/bin/env bash

## Mukarram Hossain
## 11/08/2020

# This bash script will do the following:
# - run cnvkit on tumour bam files using a pooled reference generated from
#   germline samples.

# source directory for the bam files
bamDir='./recalBams'

# we will read a text file (tumourBams.txt) with the name of all the tumour files
while IFS= read -r TSAMPLE; do

# CNVkit will be submitted to the batch system in parallel
qsub -v TSAMPLE=${TSAMPLE},BAMDIR=${bamDir} <<-"EOF"
  
#!/usr/bin/env bash

#PBS -l nodes=1:ppn=8,mem=40gb
#PBS -l walltime=50:00:00 
#PBS -j oe
#PBS -N cnvkitTumour

ml apps/cnvkit/0.9.3
ml apps/R/4.0.3

cd $PBS_O_WORKDIR

# run cnvkit on individual tumor bam files

cnvkit.py batch ${BAMDIR}/${TSAMPLE} \
-r ./Resources/cdx_gl_reference.cnn \
--processes $PBS_NP \
--output-dir cdx_copy_cnvkit/
  
  
EOF

done < ../tumourBams.txt