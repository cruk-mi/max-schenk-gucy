#!/usr/bin/env python3

# Author: Mukarram Hossain
# Date: 30/03/2021

# This python script will do the following:
# - Take two MAF as input, e.g. baseline and progression
# - will output three files:
# - - private entries in MAF_1
# - - private entries in MAF_2
# - - shared entries in MAF_1 and MAF_2
# These MAF files then can be used to draw upset plots and
# find number of private and shared variants

# USAGE: getPrivateSharedVariants.py -i maf_1 -j maf_2 -d ./ -p outprefix

# import necessary packages
import os, sys

# import argparse to get named arguments
import argparse

#*************************************
def dir_path(string):
  '''
  Checks if the argument is a valid directory path or not
  '''
  if os.path.isdir(string):
    return string
  else:
    #raise NotADirectoryError(string)
    sys.exit('Error: the directory {} not found, program is exiting.'.format(string))


#*************************************
def file_path(string):
  '''
  Checks if the argument is a valid file or not
  '''
  if os.path.isfile(string):
    return string
  else:
    #raise FileNotFoundError(string)
    sys.exit('Error: input file {} not found, program is exiting.'.format(string))


#***************************************
def createMAFDictionary(mafList):
  '''
  This function will create a dictionary from a list of MAF lines
  The keys of the dictionary will be a concatenation of gene, chromosome, position among others
  The value will be the whole line
  The dictionary will be returned to the calling statement
  '''

  mafDict = {}

  for line in mafList:
    tokens = line.split()
    dictKey = ':'.join(tokens[0:8])

    if dictKey not in mafDict:
      mafDict[dictKey] = line

  return mafDict

#*************************************
def getArguments():
  '''
  parses all the command line arguments from the calling script
  '''

  parser = argparse.ArgumentParser(description='getPrivateSharedMAF: creates MAF files for private and shared variants between two MAF files',
                                    formatter_class=argparse.RawTextHelpFormatter)

  # now adding the arguments to the parser
  parser.add_argument('-i','--maf1',help='Input MAF file 1',required=True,type=file_path)
  parser.add_argument('-j','--maf2',help='Input MAF file 2',required=True,type=file_path)
  parser.add_argument('-d','--outDir',help='Path for the output directory',default=os.getcwd(),type=dir_path)
  parser.add_argument('-p','--prefix',help='Prefix for the output files e.g. file1_file2',required=True)

  arguments = parser.parse_args()

  return arguments


# here starts the main body of the script
if __name__=='__main__':

  # get the command line arguments
  args = getArguments()

  #print(args.input,args.outDir)

  # print the file names
  #print('Input MAF file 1: {}'.format(args.maf1))
  #print('Input MAF file 2: {}'.format(args.maf2))

  # the prefix should have two part separated by an underscore
  try:
      prefix1, prefix2 = args.prefix.split('_')
  except ValueError as e:
      msg = 'The prefix should have two parts separated by "_" \n'
      sys.exit(msg)

  # now we will read the files and save them into two list
  maf1 = [line.strip() for line in open(args.maf1)]
  maf2 = [line.strip() for line in open(args.maf2)]

  # we can get the first two lines as headers
  headers = maf1[:2]

  # we will create dictionaries with variant infos as keys and lines as values
  # we can then go through the dictionaries and create private and shared files
  maf1Dict = createMAFDictionary(maf1[2:])
  maf2Dict = createMAFDictionary(maf2[2:])


  # create three lists for private and shared MAF entries
  maf1Private = headers.copy()
  maf2Private = headers.copy()
  mafShared = headers.copy()

  #print('\n'.join(maf1Private))
  # now we traverse the dictionaries
  #print(len(maf1Dict.keys() - maf2Dict.keys()) )
  #print(len(maf2Dict.keys() - maf1Dict.keys()) )
  #print(len(maf1Dict.keys() & maf2Dict.keys()) )

  for dictKey in (maf1Dict.keys() - maf2Dict.keys()):
      maf1Private.append(maf1Dict[dictKey])

  for dictKey in (maf2Dict.keys() - maf1Dict.keys()):
      maf2Private.append(maf2Dict[dictKey])

  for dictKey in (maf1Dict.keys() & maf2Dict.keys()):
      mafShared.append(maf1Dict[dictKey])


  # We now write the files
  oName1 = os.path.join(args.outDir,prefix1 + '_private.maf')
  with open(oName1,'w') as oh:
      oh.write('\n'.join(maf1Private))

  oName2 = os.path.join(args.outDir,prefix2 + '_private.maf')
  with open(oName2,'w') as oh:
      oh.write('\n'.join(maf2Private))

  oName3 = os.path.join(args.outDir,args.prefix + '_shared.maf')
  with open(oName3,'w') as oh:
      oh.write('\n'.join(mafShared))
