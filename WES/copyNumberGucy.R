# first we load the libraries 
library(pheatmap)
library(RColorBrewer)
library(tidyverse)
library(dplyr)
library(purrr)

# the home directory for GUCY project
dataDir <- "./"


# define a function to extract log2 copy number ratios for a set of genes
# from a cnvkit output file 
extractCopyNumberRatiosGene <- function(fName,genePattern){
  print(fName)
  cnr <- read.delim(fName,header=T)
  gucy <- cnr %>% filter(str_detect(gene,genePattern)) %>% dplyr::select(log2)
  return(gucy)
}


#### copy number ratios plot ####
# we will read copy number calls from *.call.cnr files
# they will be arranged in a data.frame
cdx.cnr <- fNames %>% purrr::map_dfc(extractCopyNumberRatiosGene,genePattern='GUCY1A1|GUCY1B1')
names(cdx.cnr) <- c('CDX3','CDX3P','CDX8','CDX8P','CDX17','CDX17P',
                   'CDX18','CDX18P','CDX20','CDX20P','CDX42','CDX42P')

#### scatter plot with facet_grid ####
# we will add a new column with indices 1:39
cdx.cnr <- cdx.cnr %>% mutate(index=1:nrow(cdx.cnr))
# we will create a long data from cdx.cnr
cdx.cnr_long <- cdx.cnr %>% tidyr::pivot_longer(!index,names_to='cdx',values_to='cnr')

# add patient information for faceting
cdx.cnr_long <- cdx.cnr_long %>% add_column(patient=rep(c(rep('Patient3',2),rep('Patient8',2),rep('Patient17',2),rep('Patient18',2),
                                                        rep('Patient20',2),rep('Patient42',2)),nrow(cdx.cnr))) 

# add a column for baseline and progression info
cdx.cnr_long <- cdx.cnr_long %>%  
  mutate(Tumour=ifelse(str_detect(cdx,'P'),'Progression','Baseline'))

cdx.cnr_long$cdx <- factor(cdx.cnr_long$cdx,
                          levels = c("CDX3", "CDX3P", "CDX8", "CDX8P", "CDX17","CDX17P",
                                     "CDX18","CDX18P","CDX20","CDX20P","CDX42","CDX42P"))

cdx.cnr_long$patient <- factor(cdx.cnr_long$patient,
                              levels = c('Patient3','Patient8','Patient17','Patient18','Patient20','Patient42'))


#### patient wise scatter plot with dots ####
myCol = brewer.pal(4,"Dark2")[2:3]
pdf(paste0(dataDir,'Figures/','Gucy_CNR_patient_dotted.pdf'),width=10,height=8)
bp <- ggplot(cdx.cnr_long,aes(x=index,y=cnr,colour=Tumour)) +
      geom_line() + scale_colour_manual(values = myCol) +
      xlab("GUCY genes") + ylab("Copy number ratio") + ylim(-2.5,2) +
      scale_x_continuous(breaks = 16,labels=NULL) +
      theme(axis.text.x=element_text(angle=90, hjust=1)) +
      geom_vline(xintercept=16,linetype="dotted")
    
    
bp <- bp + facet_grid(patient ~ .)
    
print(bp)
dev.off()
    
